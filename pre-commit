#!/usr/bin/ruby -W0
#!/usr/bin/env ruby -W0
# encoding: utf-8

# git-checkstyle-hook: Pre-commit hook for running checkstyle on changed Java sources
#
# Copyright (C) 2012  Miguel A. Baldi Hörlle <miguel.horlle@gmail.com>
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------------------------------
#
# Quickstart guide:
#
# To use this you need:
# 1. checkstyle's jar file somewhere
# 2. a checkstyle XML check file somewhere
# 3. To configure git:
#   * git config --add checkstyle.jar <location of jar>
#   * git config --add checkstyle.checkfile <location of checkfile>
#   * git config --add java.command <path to java executale> [optional
#     defaults to assuming it's in your path]
# 4. Put this in your .git/hooks directory as pre-commit
#
# Now, when you commit, you will be disallowed from doing so
# until you pass your checkstyle checks.
# This hook will show you all violations of style for you to fix
if RUBY_VERSION =~ /1.9/
  Encoding.default_external = Encoding::UTF_8
  Encoding.default_internal = Encoding::UTF_8
end

class DiffStat
  attr_accessor :lines, :file

  def initialize(file_name)
    @file = file_name
    @lines=[]
  end

  def add_line(line_no, line_range)
    @lines=[line_no]
    unless line_range.nil?
      line_range.times do |i|
        @lines<<(line_no=line_no+1)
      end
    end
  end

  def lines
    @lines.uniq
  end

  def empty?
    @lines.empty?
  end

  def to_s
    return "Violation::#{@file}\n\t #{@lines}"
  end
end

class Violation
  attr_accessor  :file, :line, :col, :message

  def initialize(file, line, col, message)
    @file = file
    @line = line
    @col = col
    @message = message
  end

  def to_s
    return "Violation::#{@file}\n\t #{@line},#{@col} - #{@message}"
  end
end

def violation_belongs_to_me?(diff, line_no)
  unless diff.empty?
    return diff.lines.include?(line_no)
  end
  return false
end

def build_violation(diff, check_line)
    line_no=check_line[1].to_i
    #puts "Line[#{line_no}]\n\tFile[#{diff.file}]\n\tLines[#{diff.lines}]"
    if violation_belongs_to_me? diff, line_no
      i = check_line.size == 4 ? 0 : 1
      col_no=check_line[2].to_i
      message="[#{check_line[2 + i].strip}] #{check_line[3 + i].strip}"
      check=Violation.new(diff.file ,line_no, col_no, message)
      return check
    end
end

def checkstyle_command(files)
  jar=%x[git config checkstyle.jar].chomp
  checkfile=%x[git config checkstyle.checkfile].chomp
  java_cmd="java -jar #{jar} -c #{checkfile} ".chomp
  cmd="#{java_cmd} #{files.join(' ')}"
  return cmd
end

def find_diff_by_file(diffs, file)
  diffs.each do |diff|
    if file.include? diff.file
      return diff
    end
  end
end

def check_modifications(diffs)
  files=Array.new
  for diff in diffs
    files << diff.file.chomp
  end
  output=%x[#{checkstyle_command(files)}]
  return process_checkstyle_output(output, diffs)
end

def process_checkstyle_output(output, diffs)
  checks=Array.new
  unless output.lines.count < 3
    for line in output.lines
      tokens = line.split(":")
      unless tokens.size < 2
        # token format:
        # <fully qualified file name>:<line number>:<column number>: <message>
        # MainApplicationContext.java:38:5: warning: Method 'persistFacade' is not designed for extension - needs to be abstract, final or empty.
        file = tokens[0]
        diff = find_diff_by_file(diffs, file)
        checks<<build_violation(diff, tokens)
      end
    end
  end
  return checks
end

def list_modifications
  hunk_pattern=/^@@ -(\d+)(,(\d+))? \+(\d+)(,(\d+))? @@/
  cmd_list_files = "git diff-index --cached --name-only --diff-filter=MA HEAD 2>&1 "
  cmd_file_diff = "git diff -U0 --cached -- "
  files=%x[#{cmd_list_files}].split(/\n/)
  diffs=[]
  for file in files
    exec=cmd_file_diff+file
    patch=%x[#{exec}]
    ranges=patch.scan hunk_pattern
    diff=DiffStat.new(file)
    for range in ranges
      diff.add_line(range[3].to_i, range[5].to_i)
    end
    diffs<<diff
  end
  return diffs
end

def print_summary(checks)
  unless checks.empty?
    puts "Checkstyle Violations::"
    #puts checks
  end
  for check in checks
      unless check.nil?
        puts "\n\tViolation::"
        puts "\t\tFile:: #{check.file}"
        puts "\t\tLine:: #{check.line} - Column:: #{check.col}"
        puts "\t\tMessage:: #{check.message}"
      end
  end
end

def execute
  puts "Checkstyle validation..."
  diffs=list_modifications()
  puts "#{diffs.size} modifications to check..."
  violations = check_modifications diffs
  violations.compact!
  unless violations.empty?
    print_summary violations
    puts "Patch rejected!"
    exit 1
  end
  puts "done!"
  exit 0
end

execute()
